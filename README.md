Дополнительный функционал для yii2-app-basic
============================================
Нужные доработки для обычного сайта

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist leonhodirev/yii2-app-basic "*"
```

or add

```
"leonhodirev/yii2-app-basic": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \leonhodirev\basic\AutoloadExample::widget(); ?>```